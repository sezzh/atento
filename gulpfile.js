var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var mainBowerFiles = require('main-bower-files');

// Static server
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("app/scss/**/*.scss", ['sass']);
    gulp.watch("app/**/*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("app/scss/index.scss")
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});

gulp.task('bower', function() {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest("./dist"))
});

gulp.task('build', ['build2'], function() {
    return gulp.src([
      "app/**/*.css",
      "app/**/*.js",
      "app/**/*.png",
      "app/**/*.jpg",
      "app/**/*.svg",
      "app/**/*.html"
    ])
    .pipe(gulp.dest("./dist"))
});

gulp.task('build2', function() {
    return gulp.src([
      "bower_components/jquery/dist/jquery.min.js",
      "bower_components/highcharts/highcharts.js",
      "bower_components/highcharts/highcharts-3d.js",
      "bower_components/highcharts/modules/exporting.js",
      "bower_components/bootstrap/dist/js/bootstrap.min.js",
      "bower_components/metisMenu/dist/metisMenu.min.js",
      "bower_components/bootstrap/dist/css/bootstrap.min.css",
      "bower_components/metisMenu/dist/metisMenu.min.css",
      "bower_components/font-awesome/css/font-awesome.min.css"
    ])
    .pipe(gulp.dest("./dist/dependencies"))
});

gulp.task('default', ['serve']);
