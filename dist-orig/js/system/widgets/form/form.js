(function() {


  "use strict";


	function Form( config ) {

	  var _this = this;

		this.callback = config.callback;
    this.title = config.title;
		this.$node = null;
    this.activeData = null;

		$.get( "js/system/widgets/form/form.html", function(template) {
			_this.template = template;
		  _this.load( config );
		});

	}


	Form.prototype = {


		load: function( config ) {
			this.$node = this.setEvents( $( this.template ) );
			config.targetNode.append( this.$node );
	  },


		setEvents: function( node, callback ) {
      node.off( "click" ).on( "click", "button", {
        handler: this
      }, this.handleEvents );
      node.off( "keyup" ).on( "keyup", "textarea", {
        handler: this
      }, this.onKeyUp );
			return node;
		},


		handleEvents: function( e ) {
      e.preventDefault();
      if( e.currentTarget.nodeName === "BUTTON" && !$(e.currentTarget).hasClass('disabled')) {
        $("#progresscontent").fadeIn('slow');
        e.data.handler.validateForm();
		  }
	  },

    onKeyUp: function( e ) {
      e.preventDefault();
      if( e.currentTarget.nodeName === "TEXTAREA" ) {
        if (this.value) {
          $("#submit").removeClass('disabled');
        } else {
          $("#submit").addClass('disabled');
        }
		  }
	  },


    validateForm: function() {

      this.activeData = [];
      var text = this.$node.find('textarea').val();
      var model = this.$node.find('select').val();

      if( text.length && model.length ) {

        var total = text.length;
        var i = 0;
        var comment = "";
        while(i < total) {
          if(text.charCodeAt(i) === 10 && text.charCodeAt(i-1) !== 10) {

            var item = {};
            item.model = model;
            item.txt = comment;
            this.activeData.push(item);
            comment = "";
          } else if(text.charCodeAt(i) !== 10) {
            comment += text.charAt(i);
          }
          i++;
        }

        if( comment.length ) {
          var item = {};
          item.model = model;
          item.txt = comment;
          this.activeData.push(item);
        }

        this.callback( this.activeData );

      }

    }


	};


  Core.registerComponent( "form", Form );


})();
