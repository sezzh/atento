var Core = function() {


	"use strict";


  /**
   * @_moduleData : context where the app controllers are stored
   * @_data : data retrieved from a service
   * @_User : 'User' constructor for creating users
   * @_Form : 'Form' constructor for creating forms
   * @_sandbox : mediator module between core and main controllers
  */
	var _moduleData = {};
  var _data = null;
	var _User = null;
  var _Counter = null;
  var _Form = null;
	var _sandbox = {

    /**
    * getComment
    *  get an instance of 'User' component
    *  @param config {{object}} config data object
    */
    getComment: function(config) {
      return new _User(config);
    },

		/**
		* getCounter
		*  get an instance of 'Counter' component
		*  @param config {{object}} config data object
		*/
		getCounter: function(config) {
			return new _Counter(config);
		},

    /**
    * getForm
    *  get an instance of 'Form' component
    *  @param config {{object}} config data object
    */
    getForm: function(config) { 
      return new _Form(config);
    },
    
    /**
    * getData
    *  retrieve data provided by service 'voc'
    *  @return _data {{object}} data object
    */
    getData: function() {
      return _data;
    },
    
    /**
    * setData
    *  set http request to 'voc'
    *  @param _data {{object}} data object
    */
    setData: function( params ) { 
      
      _moduleData[ "voc" ].instance.get(params, function( status, data, name ) {
        _data = data;
        _moduleData[ "users" ].instance.create( _data );
      });
			  
    },
		
    /**
    * listen
    *  registers a main controller in order to respond to a custom event
		*  fired from an external context
    *  @param eventsNames {{array}} list of string names representing the action to be listened
		*  @param callback {{function}} action to be run when a custom event is fired
		*  @param context {{object}} the context relative to the module subscribed to the custom event
    */
		listen: function( eventsNames, callback, context ) {

			var eventNamesStr = eventsNames.join();
			$( "body" ).on( eventNamesStr, {
				handler: context
			}, callback );

		},

    /**
    * notify
    *  fires a custom event when its linked action is run somewhere
    *  @param note {{object}} data object
    */
		notify: function( note ) {
			$( "body" ).trigger( note.type, [ note ] );
		}

	};


	return {


		register: function( moduleId, creator ) {

			_moduleData[ moduleId ] = {
				creator: creator,
				instance: null
			};

		},


    registerComponent: function( id, constructor ) {
      switch(id) {
        case "user":
          _User = constructor;
          break;
				case "counter":
					_Counter = constructor;
					break;
        case "form":
          _Form = constructor;
          break;
      }

    },


		start: function( moduleId ) {

			_moduleData[ moduleId ].instance = _moduleData[ moduleId ].creator( _sandbox );
			_moduleData[ moduleId ].instance.init();

		},


		startAll: function() {

			var moduleId = null;
			for( moduleId in _moduleData ) {
				if( _moduleData.hasOwnProperty(moduleId) ) {
					this.start(moduleId);
				}
			}

		},

		
		
		/*getData: function( callback ) {
			
      var _this = this;
			var mockup = [
			  {
          model: "VoC-Insurance_es",
          txt: "Foro	Ciberimperio	2013-01-08 13:17:00	Seguros Nuez otra Estafa Publicitaria. Nuez es una empresa que como otras tantas del sector se dedica a la estafa publicitaria, tomando al cliente por un borrego imbécil. Otra vez y ya son muchas se emplean tácticas machaconas, abusivas y engañosas de publicidad para ofertar un producto del que no pueden responder y del que es imposible contratar sin ningún tipo de explicación más que el llamar a un 902 con un coste desorbitado y no obteniendo ningún resultado más que la perdida de tiempo y dinero. PARECE MENTIRA QUE EN EL SIGLO EN EL QUE NOS ENCONTRAMOS SE SIGA ABUSANDO Y ENGAÑANDO TAN BURDAMENTE."
        },
				{
          model: "VoC-Insurance_es",
          txt: "Estoy muy satisfecho con el servicio"
        },
				{
          model: "VoC-Insurance_es",
          txt: "Cuando las cosas salen mal uno se debe quejar, pero cuando te las solucionan eficazmente hay que dar la enhorabuena. Gracias @asisa_hablamos"
        },
				{
          model: "VoC-Insurance_es",
          txt: "@asisa_hablamos esperando a que su 'gabinete de expertos médicos' me autorice una 2da reso de tobillo xq hay carencia de 6meses!! #failAsisa https://twitter.com/dhelgue/status/699257417335447552)"
        },
				{
          model: "VoC-Insurance_es",
          txt: "@asisa_hablamos ¿Donde os puedo hacer llegar una queja de un centro médico?"
        }
			];
      
      _moduleData[ "voc" ].instance.get(mockup, function( status, data, name ) {
        _data = data;
        _moduleData[ "users" ].instance.create( _data );
      });
					
		},*/
		


		init: function() {
      
			var _this = this;
      this.startAll();
      //_this.getData();
		
		}


	};



}();


$( window ).load( function() {
  Core.init();
} );
