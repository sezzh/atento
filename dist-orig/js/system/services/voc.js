Core.register( "voc", function( sandbox ) {


	"use strict";


  var _NAME = "voc";
  var _status = null;
  var _DATA = [];
  var _url = "http://sandbox.meaningcloud.com/deepcategorization-1.0?";


	/**
	 * _delegate
	 *  helps to run an action on the indicated context
	*/
  var _delegate = function( scope, func ) {

    var args = Array.prototype.slice.call( arguments, 2 );
    return function() {
      return func.apply( scope, Array.prototype.slice.call( arguments ).concat( args ) );
    };

  };


	/**
	 * _validate
	 *  validates that the param values to send in the http GET request have an expected format
	*/
  var _validate = function( callback, params ) {
		debugger
    params[0].key = "4441f07a2b7cdff107881f5009f87274";
    params[0].model = ( typeof params[0].model === 'string' ) ? params[0].model : null;
    params[0].txt = ( typeof params[0].txt === 'string' ) ? params[0].txt : null;

    if( params[0].model !== null && params[0].txt !== null ) {
      callback( true, params );
    }
    else {
      callback( false );
    }

  };


	/**
	 * _setHttpRequest
	 *  validates params for the 'GET' call and runs the http request to 'voc'
	*/
  var _setHttpRequest = function( callback, config ) {

    var setCall = function( isSuccess, params ) {
			debugger

      if ( isSuccess ) {

        HttpRequest.post({
	        url: _url,
          params: "key=" + encodeURIComponent(params[0].key) + "&model=" + encodeURIComponent(params[0].model) + "&txt=" + encodeURIComponent(params[0].txt),
	        responseType: "json",
	        dataType: "json",
	        timeout: 5000,
	        callback: _delegate( this, _evaluateResponse, callback, params )
        });

      }
      else {
        callback( -1, null, _NAME );
      }

    };

    _validate( setCall, config );

  };


	/**
	 * _evaluateResponse
	 *  makes sure that the data retrieved from the service has the expected structure
	*/
	var _evaluateResponse = function( data, callback, params ) {

		var isValidated = ( data.status === undefined ) ? false : ( typeof Number(data.status.code) === 'number' ) ? true : false;
		isValidated = ( !isValidated ) ? isValidated : ( data["category_list"] !== undefined ) ? true : false;
		if( isValidated ) {
			_status = Number(data.status.code);
			_setData( data, params[0] );
			_dispatchResponse(callback, params);
		}
		else {
			_status = -1;
			callback( _status, null, _NAME );
		}

	};


	/**
	 * _setData
	 *  parses raw data retrieved from the service and stores in '_DATA'
	*/
	var _setData = function( data, params ) {

		var parsedData = {};
		var i = 0;
		var total = data["category_list"].length;
		var splitList = null;
		var topic = null;
		var polarity = "NoSentiment";

		var POLARIT_Y = "Polarity";

		while(i < total) {
			splitList = data["category_list"][i].code.split("#");
			topic = splitList[0];
			polarity = ( POLARIT_Y === topic ) ? splitList[1] : polarity;
			data["category_list"][i].topic = topic;
			i++;
		}

		parsedData["action"] = "";
		parsedData["author"] = "";
		parsedData["source"] = "";
		parsedData["comment"] = params.txt;
		parsedData["polarity"] = polarity;
		parsedData["category_list"] = data["category_list"];

		_DATA.push(parsedData);

	};


	/**
	 * _dispatchResponse
	 *  eliminates object in first position from 'params' array, containing the param values
	 *  related to data which has already been successfully retrieved and parsed:
	 *  Once the item has been deleted:
	 *  -> there're still params left: starts a new http request with the values in the new pos 0
	 *  -> No params left: runs action related to the context where the communication with the 'voc'
	 *     has been set from
	*/
	var _dispatchResponse = function( callback, params ) {

		params.splice(0, 1);
		if( params.length ) {
			_setHttpRequest( callback, params );
		}
		else {
		  if( _status === 0 ) {
				callback( _status, _DATA, _NAME );
			}
			else {
				callback( -1, null, _NAME );
			}
		}

	};


	return {


    /**
		 * init
		 *  init 'voc' module by default
		*/
		init: function() {
    },

		/**
		 * get
		 *  makes sure that 'params' has the expected format in order to do an http GET request to 'voc' service
		*/
		get: function( params, callback ) {

      _status = null;
      _DATA = [];

      if ( Array.isArray( params ) ) {
        _setHttpRequest( callback, params );
      }
      else {
        callback( -1, null, _NAME );
      }

		}


	};


});
