Core.register("input", function( sandbox ) {
	
	"use strict";
  
  var _instance = null;
  
	return {
		
		
		init: function() {
		
      var _this = this;
      
      _instance = sandbox.getForm({
        targetNode: $( "#main-form" ),
        title: "Demo",
        callback: function( response ) {
          _this.isResponse(response);
        }
      });
      
		},
    
    
    isResponse: function( response ) { console.log(response);
      
      sandbox.setData(response);
      
    },
    
    
    create: function( data ) {
    },
		
		
		handle: function( note ) { 
		}
		
		
	};
	
	
});
