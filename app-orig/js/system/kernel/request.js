var HttpRequest = function() {
	
	
	"use strict";
	
	var _XHR = null;
	var _xhrTimeout = null;
	
	
	/**
	 * _dispatch
	*/
	var _dispatch = function( dataType ) {
		
		var response = -1;
		
    switch ( dataType ) {
      case "html":
      case "text":
        response = _XHR.response;
        break;
      case "json":
				try {
          if( typeof _XHR.response === 'object' && _XHR.response != null ) {
						response = _XHR.response;
					}
					else if( typeof _XHR.response === 'string' ) {
						response = JSON.parse( _XHR.response );
					}
        }
        catch ( error ) { 
          return response;
        }
        break;
      case "xml":
        response = _XHR.responseXML;
        break;
      default:
        response = -1;
    }
		
		return response;
		
	};
	
	
	/**
	 * _retrieve
	*/
	var _retrieve = function( dataType ) {
		
		var status = _XHR.status;
    var response = -1;
		
    switch ( status ) {
      case 200:
      case 201:
      case 202:
        response = _dispatch( dataType );
        break;
      case 0:
        if ( _XHR.response != null ) {
          response = _dispatch( dataType );
        }
        break;
    }
		
		return response;
		
	};
	
	
	/**
	 * _call
	*/
	var _call = function( method, params ) {
		
		_XHR = new XMLHttpRequest();
		
		if ( _XHR.withCredentials !== undefined ) {
			
			try {
      
        $.ajax({
          url: params.url,
          type: method,
          data: params.params,
          success: function (response) { console.log(response);
            params.callback( response );              
          },
          error: function(jqXHR, textStatus, errorThrown) {
            params.callback( -1 );
          }
        });
        
      }
      catch ( error ) {
        params.callback( -1 );
      }
			
		}
		
	};
	
	
	return {
		
		get: function( params ) {
			_call( "GET", params )
    },
    
    post: function( params ) { console.log(params);
			_call( "POST", params )
    }
		
	}
	
	
}();






















