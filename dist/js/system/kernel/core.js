var Core = function() {


	"use strict";


  /**
   * @_moduleData : context where the app controllers are stored
   * @_data : data retrieved from a service
   * @_User : 'User' constructor for creating users
   * @_Form : 'Form' constructor for creating forms
   * @_sandbox : mediator module between core and main controllers
  */
	var _moduleData = {};
  var _data = null;
	var _User = null;
  var _Counter = null;
  var _Form = null;
	var _sandbox = {

    /**
    * getComment
    *  get an instance of 'User' component
    *  @param config {{object}} config data object
    */
    getComment: function(config) {
      return new _User(config);
    },

		/**
		* getCounter
		*  get an instance of 'Counter' component
		*  @param config {{object}} config data object
		*/
		getCounter: function(config) {
			return new _Counter(config);
		},

    /**
    * getForm
    *  get an instance of 'Form' component
    *  @param config {{object}} config data object
    */
    getForm: function(config) { 
      return new _Form(config);
    },
    
    /**
    * getDataByFilter
    *  retrieve data provided by service 'voc'
    *  @return _data {{object}} data object
    */
    getDataByFilter: function() {
      var result = _moduleData[ "voc" ].instance.filterByPolarity( "Positive" );
      _moduleData[ "users" ].instance.create( result );
    },
    
    /**
    * setData
    *  set http request to 'voc'
    *  @param _data {{object}} data object
    */
    setData: function( params ) { 
      $("#progresscontent").fadeIn('slow');
      _moduleData[ "voc" ].instance.get(params, function( status, data, name ) {
        _data = data;
        $("#progresscontent").fadeOut('slow');
        _moduleData[ "users" ].instance.create( _data );
      });
    },
		
    /**
    * listen
    *  registers a main controller in order to respond to a custom event
		*  fired from an external context
    *  @param eventsNames {{array}} list of string names representing the action to be listened
		*  @param callback {{function}} action to be run when a custom event is fired
		*  @param context {{object}} the context relative to the module subscribed to the custom event
    */
		listen: function( eventsNames, callback, context ) {
			var eventNamesStr = eventsNames.join();
			$( "body" ).on( eventNamesStr, {
				handler: context
			}, callback );
		},

    /**
    * notify
    *  fires a custom event when its linked action is run somewhere
    *  @param note {{object}} data object
    */
		notify: function( note ) {
			$( "body" ).trigger( note.type, [ note ] );
		}

	};


	return {

    
    /**
     * register
     *  registers a module in '_moduleData' object
     *  @param moduleId {{string}} module name
     *  @param creator {{function}} module
    */
		register: function( moduleId, creator ) {

			_moduleData[ moduleId ] = {
				creator: creator,
				instance: null
			};

		},
    

    /**
     * registerComponent
     *  registers a widget component
     *  @param moduleId {{string}} module name
     *  @param creator {{function}} module
    */
    registerComponent: function( moduleId, creator ) {
      
      switch(moduleId) {
        case "user":
          _User = creator;
          break;
				case "counter":
					_Counter = creator;
					break;
        case "form":
          _Form = creator;
          break;
      }

    },

    
    /**
     * start
     *  initializes a module for first time
     *  @param moduleId {{string}} module name
    */
		start: function( moduleId ) {

			_moduleData[ moduleId ].instance = _moduleData[ moduleId ].creator( _sandbox );
			_moduleData[ moduleId ].instance.init();

		},

    
    /**
     * startAll
     *  initializes available modules stored in '_moduleData'
    */
		startAll: function() {

			var moduleId = null;
			for( moduleId in _moduleData ) {
				if( _moduleData.hasOwnProperty(moduleId) ) {
					this.start(moduleId);
				}
			}

		},
    
    
    /**
     * init
     *  initializes core for first time
    */
		init: function() {
      
			var _this = this;
      this.startAll();
		
		}


	};



}();


$( window ).load( function() {
  Core.init();
} );
