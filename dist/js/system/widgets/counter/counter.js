(function() {

  "use strict";

	function Counter( config ) {
	  var _this = this;


		this.callback = config.callback;
    this.title = config.title;
		this.$node = null;

		$.get( "js/system/widgets/counter/counter.html", function(template) {
			_this.template = template;
		  _this.load( config );
		});

	}


	Counter.prototype = {


		load: function( config ) {
      console.log("load");
		  this.template = this.template.replace( "$$_title_$$", config.title );
			this.$node = this.setEvents( $( this.template ) );

			config.targetNode.append( this.$node );

	  },


		setEvents: function( node, callback ) {
      node.off( "click" ).on( "click", {
        handler: this
      }, this.handleEvents );
			return node;
		},


		handleEvents: function( e ) {
		    e.data.handler.callback( e.data.handler.title );
			  return false;
	  }


	};


  Core.registerComponent( "counter", Counter );


})();
