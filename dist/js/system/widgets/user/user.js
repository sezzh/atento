(function() {

  "use strict";

  function _listclass(clases) {
    var label;
    var e;
    var claseli = "";
    var es = {
      Bank: 'Banca',
      Channel: 'Canal',
      Condition: 'Condición',
      CustomerService: 'Atención al cliente',
      Operation: 'Operación',
      Product: 'Producto',
      Quality: 'Calidad',
      Action: 'Acción',
      Company: 'Compañia',
      Polarity: 'Polaridad'
    };

    var clases_icon = {
      Operation: "<span class='glyphicon glyphicon-wrench fa-1x' aria-hidden='true'></span>",
      Quality: "<span class='glyphicon glyphicon-star fa-1x' aria-hidden='true'></span>",
      CustomerService: "<span class='glyphicon glyphicon-info-sign fa-1x' aria-hidden='true'></span>",
      Channel: "<span class='glyphicon glyphicon-phone fa-1x' aria-hidden='true'></span>",
      Bank: "<span class='glyphicon glyphicon-piggy-bank fa-1x' aria-hidden='true'></span>",
      Condition: "<span class='glyphicon glyphicon-tasks fa-1x' aria-hidden='true'></span>",
      Company: "<span class='glyphicon glyphicon-briefcase fa-1x' aria-hidden='true'></span>",
      Product: "<span class='glyphicon glyphicon-barcode fa-1x' aria-hidden='true'></span>",
      Action: "<span class='glyphicon glyphicon-play fa-1x' aria-hidden='true'></span>",
    };
    for (e in clases) {
      var clase = clases[e];
      if(clase.topic !== 'Polarity' && clase.topic !== 'Action')
        claseli += '<li class="clase">'+clases_icon[clase.topic]+'<span><label class="labelclass">'+es[clase.topic]+':</label><span>'+clases[e].label+'</span></span></li>';

    }
    return claseli;
  }

  function _getPolaridadObject(polaridad){
    var polaridadObjects = {
      StrongNegative: {
        classCss: 'negativestrong',
        label: 'Negativo fuerte'
      },
      Negative: {
        classCss: 'negative',
        label: 'Negativo'
      },
      Neutral: {
        classCss: 'neutral',
        label: 'Neutral'
      },
      Positive: {
        classCss: 'positive',
        label: 'Positivo'
      },
      StrongPositive: {
        classCss: 'positivestrong',
        label: 'Positivo fuerte'
      },
      NoSentiment: {
        classCss: 'nofeelings',
        label: 'Sin sentimiento'
      }
    };
    var result = polaridadObjects[polaridad] || {classCss: 'nofeelings', label: 'Sin sentimiento'};
    return result;
  }

  function _getAccion(clases) {
    var e = 0;
    var accionesli = '';
    var hayAccion = false;

    for (e in clases) {
      var clase = clases[e];
      if(clase.topic === 'Action') {
        hayAccion = true;
        accionesli += '<li class="clase">'+clase.label+'</li>';
      }
    }
    if (hayAccion) {
      accionesli = '<label class="label-acciones">Acciones:</label><ul>'+accionesli+'</ul>';
    }

    return accionesli;
  }

	function User( config ) {
	  var _this = this;
    var claselis = _listclass(config.clases);
    var polaridadObject = _getPolaridadObject(config.polaridad);
    var accion = _getAccion(config.clases);


		this.callback = config.callback;
    this.title = config.title;
		this.$node = null;

		$.get( "js/system/widgets/user/user.html", function(template) {
      var htmlResult = template;
      htmlResult = htmlResult.replace('[[title]]', config.title);
      htmlResult = htmlResult.replace('[[classCss]]', polaridadObject.classCss);
      htmlResult = htmlResult.replace('[[claselis]]', claselis);
      htmlResult = htmlResult.replace('[[accion]]', accion);
			_this.template = htmlResult;
		  _this.load( config );
		});

	}


	User.prototype = {


		load: function( config ) {
		  this.template = this.template.replace( "$$_title_$$", config.title );
			this.$node = this.setEvents( $( this.template ) );

			config.targetNode.append( this.$node );
      //$('#progresscontent').fadeOut('slow');
      $('#page-wrapper').slideDown('slow', function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 500);
      });

	  },


		setEvents: function( node, callback ) {
      node.off( "click" ).on( "click", {
        handler: this
      }, this.handleEvents );
			return node;
		},


		handleEvents: function( e ) {
		    e.data.handler.callback( e.data.handler.title );
			  return false;
	  }


	};


  Core.registerComponent( "user", User );


})();
