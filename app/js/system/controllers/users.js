Core.register("users", function( sandbox ) {

	"use strict";

	return {


		init: function() {

			sandbox.listen( [ "user-input" ], this.handle, this );


		},


    create: function( data ) {

			/*
			*  Creación de las tarjetas
			*/
			var cards = [];
			var card;
			var i;
			var polaridad;
			$('#cards').html('');
			for (i in data) {
				var message = data[i];

				polaridad =  message.polarity || 'nofeelings';

				card = sandbox.getComment({
					title: message.comment,
					polaridad: polaridad,
					clases: message.category_list,
					targetNode: $( "#cards" ),
					callback: function() {

					}
				});

				cards.push(card);
			}

			/*
			*  Creación de los contadores
			*/
			var contadores = {
				StrongNegative: 0,
				Negative: 0,
				Neutral: 0,
				Positive: 0,
				StrongPositive: 0,
				NoSentiment: 0
			};

			var messageObject = {};
			i = 0;
			for (i in data) {
				messageObject = data[i];

				polaridad =  messageObject.polarity || 'nofeelings';

				contadores[polaridad]++;

				cards.push(card);
			}
			var $counters = $("#counters");
			$("#StrongNegative .huge", $counters).html(contadores.StrongNegative);
			$("#Negative .huge", $counters).html(contadores.Negative);
			$("#Neutral .huge", $counters).html(contadores.Neutral);
			$("#Positive .huge", $counters).html(contadores.Positive);
			$("#StrongPositive .huge", $counters).html(contadores.StrongPositive);
			$("#NoSentiment .huge", $counters).html(contadores.NoSentiment);

			$('#container').highcharts({
					chart: {
						width: '400',
						type: 'pie',
						options3d: {
								enabled: true,
								alpha: 45
						}
					},
					title: {
							text: 'Polaridad'
					},
					subtitle: {
							text: ''
					},
					plotOptions: {
							pie: {
									innerSize: 100,
									depth: 45
							}
					},
					series: [{
							name: 'Delivered amount',
							data: [
									['Positivo fuerte', contadores.StrongPositive],
									['Positivo', contadores.Positive],
									['Neutro', contadores.Neutral],
									['Negativo', contadores.Negative],
									['Negativo fuerte', contadores.StrongNegative],
									['Sin sentimiento', contadores.NoSentiment]

							]
					}]
			});

    },


		handle: function( e, note ) {

      switch(note.type) {
				case "user-input":
				  console.log(note.data);
				  break;
				default:
				  console.log(note);
			}

		}

	};

});
